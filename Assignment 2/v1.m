clear;
clc;
close all;
syms k T N V m h;
R=m*k;
Q_tr=V*(2*pi*m*k*T/h^2)^1.5;
Q_rot=1;
Q=Q_tr*Q_rot;
F=N*k*T*log(Q/N)+1;
E=N*k*T^2*diff(log(Q),T);
H=E+N*k*T;
Cp=diff(H,T)
Cv=diff(E,T)
gamma=Cp/Cv
% N=6.023*10^23;
% k=1.38*10^-23;
% eval(Cp)
% eval(Cv)
% eval(gamma)
