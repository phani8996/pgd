clear;
clc;
close all;
load 'Kp_values.mat';
P=[1 0.5 4];
for j=1:3
    for i=1:2
        Kpno=10^Kp_values(i,4);
        Kpn=10^Kp_values(i,5);
        Kpo=10^Kp_values(i,6);
        fun=@(x)eql_eqns(x,P(j),Kpn,Kpno,Kpo);
        x0=zeros(1,5);
        x=fsolve(fun,x0,optimoptions('fsolve','Display','off'));
        fprintf('Equilibrium mole fractions at P=%.2fbar,T=%dK:\n',P(j),Kp_values(i,1));
        fprintf('Xn2=%.8f\nXo2=%.8f\nXno=%.8f\nXn=%.8f\nXo=%.8f\n\n',x(1),x(2),x(3),x(4),x(5));
    end
end
