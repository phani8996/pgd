function F = eql_eqns(x,P,Kpn,Kpno,Kpo)
    N_n2=0.79;
    N_o2=0.21;
    F(1) = x(1)+x(2)+x(3)+x(4)+x(5)-1;
    F(2) = 2*N_o2*(2*x(1)+x(3)+x(4))-2*N_n2*(2*x(2)+x(3)+x(5));
    F(3)= x(3)-Kpno*sqrt(x(1)*x(2));
    F(4)= x(4)-Kpn*sqrt(x(1)/P);
    F(5)= x(5)-Kpo*sqrt(x(2)/P);
end